var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    bodyParser = require('body-parser'),
    controller = require('./controller')

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header("Access-Control-Allow-Headers", 
    "Origin, X-Requested-With, x-access-token, Content-Type, Accept, cache-control");
  next();
});

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())



var routes = require('./routes')
routes(app)

app.listen(port)
console.log('hpp service: ' + port)