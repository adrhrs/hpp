'use strict'

exports.ok = function(values, res) {
  var data = {
      'status': 200,
      'values': values
  }
  res.json(data)
  res.end()
}

exports.fail = function(values, query , res) {
  var data = {
      'status': 500,
      'msg': values,
      'query': query
  }
  res.json(data)
  res.end()
}