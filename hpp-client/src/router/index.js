import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Hpp from "../views/Hpp.vue";



Vue.use(VueRouter);
Vue.prototype.$host = 'http://localhost:3000'

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/hpp",
    name: "hpp",
    component: Hpp
  },

];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
