'use strict'

var response = require('./res')
var connection = require('./conn')
var limit = 10

exports.index = function(req, res) {
    response.ok("HPP Service", res)
}


exports.listGeneric = function(req, res) {

    var table_name = req.params.table_name
    var page = parseInt(req.params.page)
    
    var offset = (page-1) * limit
    var where = ' WHERE is_deleted = 0 '
    var query = 'SELECT *, DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i:%s") as timestamp FROM ' 
    + table_name + where + 'ORDER BY ID DESC' + ' LIMIT ' + limit + ' OFFSET ' + offset
    
    connection.query(query, function (error, rows, fields){
        if(error){
            response.fail(error.sqlMessage, query, res)
        } else{
            response.ok(rows, res)
        }
    })
}

exports.conditionalGeneric = function(req, res) {

    var table_name = req.params.table_name

    var attr = Object.keys(req.body)[0]
    var val = "'" + Object.values(req.body)[0] + "'"
    var limit = 100
    var page = parseInt(req.params.page)
    var offset = (page-1) * limit
    var where = ' WHERE is_deleted = 0' + ' AND ' + attr + ' = ' + val 
    var query = 'SELECT *, DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i:%s") as timestamp FROM ' 
    + table_name + where + 'ORDER BY ID DESC' + ' LIMIT ' + limit + ' OFFSET ' + offset
    
    connection.query(query, function (error, rows, fields){
        if(error){
            response.fail(error.sqlMessage, query, res)
        } else{
            response.ok(rows, res)
        }
    })
}

exports.findGeneric = function(req, res) {
    
    var table_name = req.params.table_name
    var id = req.params.id
    var where = ' and is_deleted = 0 '
    var query = 'SELECT *, DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i:%s") as timestamp FROM ' 
    + table_name + ' where id = ' + id + where

    connection.query(query,function (error, rows, fields){
        if(error){
            response.fail(error.sqlMessage, query, res)
        } else{
            response.ok(rows, res)
        }
    })
}

exports.createGeneric = function(req, res) {
    
    var table_name = req.params.table_name
    var values = Object.values(req.body)
    var column = Object.keys(req.body) 

    var valfix = []

    values.forEach((n, i) => { valfix[i] = "'" + n + "'"})

    var val = "(" + valfix.toString() + ")"
    var col = "(" + column.toString() + ")"

    var query = "INSERT INTO " + table_name + " " + col + " values " + val

    console.log(query)

    connection.query(query,function (error, rows, fields){
        if(error){
            response.fail(error.sqlMessage, query, res)
        } else{
            response.ok("Successfully create data on " + table_name, res)
        }
    })
}

exports.updateGeneric = function(req, res) {
    
    var table_name = req.params.table_name
    var id = req.params.id

    var values = Object.values(req.body)
    var column = Object.keys(req.body) 

    var valfix = []
    values.forEach((n, i) => { valfix[i] = "'" + n + "'"})

    var updated = [] 

    for (var i = 0; i < valfix.length; i++) {

        if(column[i] != "timestamp" ) {
            var upt = column[i] + " = " + valfix[i]
            updated.push(upt)
        }
        
    }

    var query = "UPDATE " + table_name + " SET " + 
    updated.toString() + " WHERE ID = " + id

    console.log(query)

    connection.query(query,function (error, rows, fields){
        if(error){
            response.fail(error.sqlMessage, query, res)
        } else{
            response.ok("Successfully update data on " + table_name, res)
        }
    })
}

