-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2019 at 06:06 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hpp_service`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(222) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `name`, `code`, `created_at`, `is_deleted`) VALUES
(3, 'Tinamou, elegant crested', '#ae3e23', '2019-12-15 13:34:19', 0),
(4, 'Pygmy possum', '#756635', '2019-12-15 13:34:19', 0),
(5, 'European wild cat', '#66584f', '2019-12-15 13:34:19', 0),
(6, 'Creeper, black-tailed tree', '#aa4f8e', '2019-12-15 13:34:19', 0),
(7, 'Glider, feathertail', '#2919c9', '2019-12-15 13:34:19', 0),
(8, 'Blue-breasted cordon bleu', '#e6ee19', '2019-12-15 13:34:19', 0),
(9, 'Uinta ground squirrel', '#0c487e', '2019-12-15 13:34:19', 0),
(10, 'Alligator, american', '#00fd16', '2019-12-15 13:34:19', 0),
(11, 'Vulture, egyptian', '#161fd5', '2019-12-15 13:34:19', 0),
(12, 'Seven-banded armadillo', '#82e19a', '2019-12-15 13:34:19', 0),
(13, 'Common mynah', '#411b48', '2019-12-15 13:34:19', 0),
(14, 'Fox, crab-eating', '#f0068e', '2019-12-15 13:34:19', 0),
(15, 'Campo flicker', '#2c8af0', '2019-12-15 13:34:19', 0),
(16, 'Steenbuck', '#20097c', '2019-12-15 13:34:19', 0),
(17, 'Oryx, beisa', '#de6dc1', '2019-12-15 13:34:19', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hpps`
--

CREATE TABLE `hpps` (
  `id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `name` varchar(123) NOT NULL,
  `is_deleted` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hpps`
--

INSERT INTO `hpps` (`id`, `article_id`, `name`, `is_deleted`, `created_at`) VALUES
(1, 9, 'repurpose integrated interfaces', 0, '2019-12-15 13:38:01'),
(2, 17, 'evolve proactive web services', 0, '2019-12-15 13:38:01'),
(3, 4, 'seize collaborative convergence', 0, '2019-12-15 13:38:01'),
(4, 11, 'deliver web-enabled niches', 0, '2019-12-15 13:38:01'),
(5, 5, 'streamline value-added experiences', 0, '2019-12-15 13:38:01'),
(6, 7, 'scale 24/7 infomediaries', 0, '2019-12-15 13:38:01'),
(7, 14, 'repurpose customized initiatives', 0, '2019-12-15 13:38:01'),
(8, 10, 'empower seamless web services', 0, '2019-12-15 13:38:01'),
(9, 16, 'leverage frictionless e-markets', 0, '2019-12-15 13:38:01'),
(10, 11, 'harness turn-key portals', 0, '2019-12-15 13:38:01'),
(11, 11, 'iterate B2B relationships', 0, '2019-12-15 13:38:01'),
(12, 11, 'mesh turn-key e-business', 0, '2019-12-15 13:38:01'),
(13, 5, 'mesh customized paradigms', 0, '2019-12-15 13:38:01'),
(14, 3, 'disintermediate open-source paradigms', 0, '2019-12-15 13:38:01'),
(15, 7, 'optimize plug-and-play metrics', 0, '2019-12-15 13:38:01'),
(16, 17, 'incentivize interactive e-markets', 0, '2019-12-15 13:38:01'),
(17, 8, 'extend web-enabled architectures', 0, '2019-12-15 13:38:01'),
(18, 14, 'harness web-enabled initiatives', 0, '2019-12-15 13:38:01'),
(19, 4, 'aggregate compelling communities', 0, '2019-12-15 13:38:01'),
(20, 13, 'leverage distributed models', 0, '2019-12-15 13:38:01'),
(21, 4, 'target killer bandwidth', 0, '2019-12-15 13:38:01'),
(22, 7, 'iterate one-to-one networks', 0, '2019-12-15 13:38:01'),
(23, 13, 'architect granular technologies', 0, '2019-12-15 13:38:01'),
(24, 16, 'deploy sticky action-items', 0, '2019-12-15 13:38:01'),
(25, 17, 'orchestrate viral relationships', 0, '2019-12-15 13:38:01'),
(26, 16, 'deploy front-end solutions', 0, '2019-12-15 13:38:01'),
(27, 13, 'empower transparent models', 0, '2019-12-15 13:38:01'),
(28, 5, 'drive sexy mindshare', 0, '2019-12-15 13:38:01'),
(29, 15, 'incentivize extensible e-markets', 0, '2019-12-15 13:38:01'),
(30, 5, 'maximize B2C systems', 0, '2019-12-15 13:38:01'),
(31, 3, 'expedite proactive action-items', 0, '2019-12-15 13:38:01'),
(32, 12, 'mesh front-end supply-chains', 0, '2019-12-15 13:38:01'),
(33, 11, 'repurpose bricks-and-clicks e-markets', 0, '2019-12-15 13:38:01'),
(34, 15, 'scale world-class bandwidth', 0, '2019-12-15 13:38:01'),
(35, 13, 'strategize collaborative synergies', 0, '2019-12-15 13:38:01'),
(36, 4, 'drive world-class e-markets', 0, '2019-12-15 13:38:01'),
(37, 16, 'unleash seamless convergence', 0, '2019-12-15 13:38:01'),
(38, 11, 'utilize user-centric e-commerce', 0, '2019-12-15 13:38:01'),
(39, 5, 'productize killer infrastructures', 0, '2019-12-15 13:38:01'),
(40, 6, 'synergize customized convergence', 0, '2019-12-15 13:38:01');

-- --------------------------------------------------------

--
-- Table structure for table `hpp_details`
--

CREATE TABLE `hpp_details` (
  `id` int(11) NOT NULL,
  `raw_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `hpp_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hpp_details`
--

INSERT INTO `hpp_details` (`id`, `raw_id`, `qty`, `hpp_id`, `created_at`, `is_deleted`) VALUES
(2, 53, 23, 37, '2019-12-15 13:41:41', 0),
(3, 96, 23, 19, '2019-12-15 13:41:41', 0),
(4, 25, 9, 32, '2019-12-15 13:41:41', 0),
(5, 75, 14, 7, '2019-12-15 13:41:41', 0),
(6, 63, 4, 2, '2019-12-15 13:41:41', 0),
(7, 44, 7, 38, '2019-12-15 13:41:41', 0),
(8, 46, 23, 21, '2019-12-15 13:41:41', 0),
(9, 35, 5, 18, '2019-12-15 13:41:41', 0),
(10, 36, 29, 29, '2019-12-15 13:41:41', 0),
(11, 13, 27, 39, '2019-12-15 13:41:41', 0),
(12, 36, 10, 6, '2019-12-15 13:41:41', 0),
(13, 56, 5, 10, '2019-12-15 13:41:41', 0),
(14, 46, 14, 2, '2019-12-15 13:41:41', 0),
(15, 99, 7, 19, '2019-12-15 13:41:41', 0),
(16, 87, 2, 36, '2019-12-15 13:41:41', 0),
(17, 94, 25, 12, '2019-12-15 13:41:41', 0),
(18, 33, 20, 18, '2019-12-15 13:41:41', 0),
(19, 79, 23, 14, '2019-12-15 13:41:41', 0),
(20, 77, 21, 38, '2019-12-15 13:41:41', 0),
(21, 21, 3, 12, '2019-12-15 13:41:41', 0),
(22, 99, 24, 9, '2019-12-15 13:41:41', 0),
(23, 65, 5, 7, '2019-12-15 13:41:41', 0),
(24, 87, 20, 35, '2019-12-15 13:41:41', 0),
(25, 69, 22, 27, '2019-12-15 13:41:41', 0),
(26, 38, 4, 28, '2019-12-15 13:41:41', 0),
(27, 42, 19, 10, '2019-12-15 13:41:41', 0),
(28, 20, 23, 22, '2019-12-15 13:41:41', 0),
(29, 79, 11, 3, '2019-12-15 13:41:41', 0),
(30, 43, 28, 2, '2019-12-15 13:41:41', 0),
(31, 15, 24, 22, '2019-12-15 13:41:41', 0),
(32, 24, 22, 23, '2019-12-15 13:41:41', 0),
(33, 9, 27, 30, '2019-12-15 13:41:41', 0),
(34, 72, 6, 22, '2019-12-15 13:41:41', 0),
(35, 32, 30, 38, '2019-12-15 13:41:41', 0),
(36, 16, 28, 24, '2019-12-15 13:41:41', 0),
(37, 68, 21, 22, '2019-12-15 13:41:41', 0),
(38, 80, 22, 28, '2019-12-15 13:41:41', 0),
(39, 21, 10, 24, '2019-12-15 13:41:41', 0),
(40, 42, 13, 9, '2019-12-15 13:41:41', 0),
(41, 51, 15, 3, '2019-12-15 13:41:41', 0),
(42, 31, 20, 29, '2019-12-15 13:41:41', 0),
(43, 45, 21, 33, '2019-12-15 13:41:41', 0),
(44, 40, 15, 8, '2019-12-15 13:41:41', 0),
(45, 19, 18, 19, '2019-12-15 13:41:41', 0),
(46, 18, 6, 4, '2019-12-15 13:41:41', 0),
(47, 27, 2, 38, '2019-12-15 13:41:41', 0),
(48, 16, 9, 35, '2019-12-15 13:41:41', 0),
(49, 10, 22, 19, '2019-12-15 13:41:41', 0),
(50, 71, 5, 40, '2019-12-15 13:41:41', 0),
(51, 61, 1, 27, '2019-12-15 13:41:41', 0),
(52, 12, 19, 15, '2019-12-15 13:41:41', 0),
(53, 57, 16, 35, '2019-12-15 13:41:41', 0),
(54, 76, 3, 25, '2019-12-15 13:41:41', 0),
(55, 39, 1, 2, '2019-12-15 13:41:41', 0),
(56, 58, 26, 1, '2019-12-15 13:41:41', 0),
(57, 30, 30, 34, '2019-12-15 13:41:41', 0),
(58, 13, 18, 3, '2019-12-15 13:41:41', 0),
(59, 88, 30, 29, '2019-12-15 13:41:41', 0),
(60, 44, 1, 36, '2019-12-15 13:41:41', 0),
(61, 21, 1, 18, '2019-12-15 13:41:41', 0),
(62, 62, 16, 15, '2019-12-15 13:41:41', 0),
(63, 99, 1, 11, '2019-12-15 13:41:41', 0),
(64, 81, 2, 22, '2019-12-15 13:41:41', 0),
(65, 55, 27, 37, '2019-12-15 13:41:41', 0),
(66, 9, 4, 23, '2019-12-15 13:41:41', 0),
(67, 78, 18, 39, '2019-12-15 13:41:41', 0),
(68, 66, 10, 24, '2019-12-15 13:41:41', 0),
(69, 46, 13, 16, '2019-12-15 13:41:41', 0),
(70, 34, 9, 1, '2019-12-15 13:41:41', 0),
(71, 28, 9, 22, '2019-12-15 13:41:41', 0),
(72, 29, 22, 37, '2019-12-15 13:41:41', 0),
(73, 9, 22, 7, '2019-12-15 13:41:41', 0),
(74, 96, 9, 15, '2019-12-15 13:41:41', 0),
(75, 60, 15, 36, '2019-12-15 13:41:41', 0),
(76, 18, 23, 6, '2019-12-15 13:41:41', 0),
(77, 88, 20, 21, '2019-12-15 13:41:41', 0),
(78, 17, 20, 39, '2019-12-15 13:41:41', 0),
(79, 81, 2, 36, '2019-12-15 13:41:41', 0),
(80, 58, 30, 7, '2019-12-15 13:41:41', 0),
(81, 13, 26, 14, '2019-12-15 13:41:41', 0),
(82, 52, 11, 7, '2019-12-15 13:41:41', 0),
(83, 70, 27, 16, '2019-12-15 13:41:41', 0),
(84, 29, 24, 21, '2019-12-15 13:41:41', 0),
(85, 49, 6, 14, '2019-12-15 13:41:41', 0),
(86, 56, 23, 39, '2019-12-15 13:41:41', 0),
(87, 7, 21, 10, '2019-12-15 13:41:41', 0),
(88, 6, 30, 35, '2019-12-15 13:41:41', 0),
(89, 97, 23, 33, '2019-12-15 13:41:41', 0),
(90, 78, 22, 12, '2019-12-15 13:41:41', 0),
(91, 47, 8, 9, '2019-12-15 13:41:41', 0),
(92, 59, 7, 8, '2019-12-15 13:41:41', 0),
(93, 85, 13, 25, '2019-12-15 13:41:41', 0),
(94, 51, 4, 37, '2019-12-15 13:41:41', 0),
(95, 95, 14, 11, '2019-12-15 13:41:41', 0),
(96, 78, 8, 14, '2019-12-15 13:41:41', 0),
(97, 94, 12, 36, '2019-12-15 13:41:41', 0),
(98, 27, 21, 5, '2019-12-15 13:41:41', 0),
(99, 93, 30, 24, '2019-12-15 13:41:41', 0),
(100, 30, 15, 38, '2019-12-15 13:41:41', 0),
(101, 56, 7, 9, '2019-12-15 13:41:41', 0),
(102, 15, 23, 22, '2019-12-15 13:41:41', 0),
(103, 42, 13, 17, '2019-12-15 13:41:41', 0),
(104, 67, 12, 19, '2019-12-15 13:41:41', 0),
(105, 17, 19, 9, '2019-12-15 13:41:41', 0),
(106, 22, 18, 26, '2019-12-15 13:41:41', 0),
(107, 6, 28, 18, '2019-12-15 13:41:41', 0),
(108, 93, 8, 40, '2019-12-15 13:41:41', 0),
(109, 28, 2, 32, '2019-12-15 13:41:41', 0),
(110, 54, 7, 14, '2019-12-15 13:41:41', 0),
(111, 58, 18, 35, '2019-12-15 13:41:41', 0),
(112, 53, 14, 34, '2019-12-15 13:41:41', 0),
(113, 72, 14, 30, '2019-12-15 13:41:41', 0),
(114, 46, 10, 30, '2019-12-15 13:41:41', 0),
(115, 51, 16, 28, '2019-12-15 13:41:41', 0),
(116, 27, 30, 26, '2019-12-15 13:41:41', 0),
(117, 96, 24, 22, '2019-12-15 13:41:41', 0),
(118, 28, 22, 24, '2019-12-15 13:41:41', 0),
(119, 83, 22, 40, '2019-12-15 13:41:41', 0),
(120, 52, 7, 31, '2019-12-15 13:41:41', 0),
(121, 13, 25, 10, '2019-12-15 13:41:41', 0),
(122, 46, 28, 13, '2019-12-15 13:41:41', 0),
(123, 83, 7, 18, '2019-12-15 13:41:41', 0),
(124, 12, 27, 8, '2019-12-15 13:41:41', 0),
(125, 37, 2, 5, '2019-12-15 13:41:41', 0),
(126, 40, 27, 11, '2019-12-15 13:41:41', 0),
(127, 77, 1, 39, '2019-12-15 13:41:41', 0),
(128, 70, 2, 38, '2019-12-15 13:41:41', 0),
(129, 44, 6, 1, '2019-12-15 13:41:41', 0),
(130, 73, 2, 36, '2019-12-15 13:41:41', 0),
(131, 16, 16, 7, '2019-12-15 13:41:41', 0),
(132, 28, 24, 36, '2019-12-15 13:41:41', 0),
(133, 43, 1, 22, '2019-12-15 13:41:41', 0),
(134, 96, 5, 8, '2019-12-15 13:41:41', 0),
(135, 27, 16, 22, '2019-12-15 13:41:41', 0),
(136, 93, 4, 18, '2019-12-15 13:41:41', 0),
(137, 98, 15, 12, '2019-12-15 13:41:41', 0),
(138, 61, 8, 25, '2019-12-15 13:41:41', 0),
(139, 72, 26, 37, '2019-12-15 13:41:41', 0),
(140, 86, 21, 10, '2019-12-15 13:41:41', 0),
(141, 46, 15, 31, '2019-12-15 13:41:41', 0),
(142, 17, 19, 2, '2019-12-15 13:41:41', 0),
(143, 90, 13, 4, '2019-12-15 13:41:41', 0),
(144, 34, 16, 19, '2019-12-15 13:41:41', 0),
(145, 13, 24, 5, '2019-12-15 13:41:41', 0),
(146, 74, 19, 16, '2019-12-15 13:41:41', 0),
(147, 46, 20, 19, '2019-12-15 13:41:41', 0),
(148, 14, 26, 32, '2019-12-15 13:41:41', 0),
(149, 66, 4, 23, '2019-12-15 13:41:41', 0),
(150, 97, 2, 17, '2019-12-15 13:41:41', 0),
(151, 13, 23, 22, '2019-12-15 13:41:41', 0),
(152, 46, 8, 37, '2019-12-15 13:41:41', 0),
(153, 12, 12, 39, '2019-12-15 13:41:41', 0),
(154, 82, 18, 4, '2019-12-15 13:41:41', 0),
(155, 36, 9, 13, '2019-12-15 13:41:41', 0),
(156, 42, 22, 28, '2019-12-15 13:41:41', 0),
(157, 27, 17, 11, '2019-12-15 13:41:41', 0),
(158, 27, 14, 13, '2019-12-15 13:41:41', 0),
(159, 11, 23, 17, '2019-12-15 13:41:41', 0),
(160, 46, 11, 15, '2019-12-15 13:41:41', 0),
(161, 7, 18, 3, '2019-12-15 13:41:41', 0),
(162, 99, 25, 29, '2019-12-15 13:41:41', 0),
(163, 75, 12, 34, '2019-12-15 13:41:41', 0),
(164, 19, 21, 16, '2019-12-15 13:41:41', 0),
(165, 29, 13, 31, '2019-12-15 13:41:41', 0),
(166, 91, 20, 23, '2019-12-15 13:41:41', 0),
(167, 60, 13, 16, '2019-12-15 13:41:41', 0),
(168, 57, 30, 12, '2019-12-15 13:41:41', 0),
(169, 7, 19, 6, '2019-12-15 13:41:41', 0),
(170, 33, 16, 20, '2019-12-15 13:41:41', 0),
(171, 56, 15, 8, '2019-12-15 13:41:41', 0),
(172, 85, 6, 25, '2019-12-15 13:41:41', 0),
(173, 98, 6, 7, '2019-12-15 13:41:41', 0),
(174, 76, 4, 20, '2019-12-15 13:41:41', 0),
(175, 9, 9, 11, '2019-12-15 13:41:41', 0),
(176, 89, 27, 13, '2019-12-15 13:41:41', 0),
(177, 19, 18, 40, '2019-12-15 13:41:41', 0),
(178, 24, 22, 2, '2019-12-15 13:41:41', 0),
(179, 10, 30, 27, '2019-12-15 13:41:41', 0),
(180, 82, 11, 2, '2019-12-15 13:41:41', 0),
(181, 99, 7, 40, '2019-12-15 13:41:41', 0),
(182, 31, 28, 30, '2019-12-15 13:41:41', 0),
(183, 100, 22, 23, '2019-12-15 13:41:41', 0),
(184, 7, 4, 27, '2019-12-15 13:41:41', 0),
(185, 99, 17, 30, '2019-12-15 13:41:41', 0),
(186, 71, 17, 26, '2019-12-15 13:41:41', 0),
(187, 13, 9, 40, '2019-12-15 13:41:41', 0),
(188, 66, 25, 17, '2019-12-15 13:41:41', 0),
(189, 72, 17, 35, '2019-12-15 13:41:41', 0),
(190, 32, 9, 12, '2019-12-15 13:41:41', 0),
(191, 87, 17, 36, '2019-12-15 13:41:41', 0),
(192, 53, 3, 20, '2019-12-15 13:41:41', 0),
(193, 25, 23, 28, '2019-12-15 13:41:41', 0),
(194, 98, 27, 7, '2019-12-15 13:41:41', 0),
(195, 26, 2, 19, '2019-12-15 13:41:41', 0),
(196, 13, 13, 22, '2019-12-15 13:41:41', 0),
(197, 25, 15, 13, '2019-12-15 13:41:41', 0),
(198, 58, 20, 29, '2019-12-15 13:41:41', 0),
(199, 41, 30, 24, '2019-12-15 13:41:41', 0),
(200, 50, 10, 12, '2019-12-15 13:41:41', 0),
(201, 30, 1, 27, '2019-12-15 13:41:41', 0),
(202, 79, 13, 16, '2019-12-15 13:41:41', 0),
(203, 49, 7, 4, '2019-12-15 13:41:41', 0),
(204, 65, 5, 30, '2019-12-15 13:41:41', 0),
(205, 39, 1, 34, '2019-12-15 13:41:41', 0),
(206, 64, 16, 16, '2019-12-15 13:41:41', 0),
(207, 9, 1, 7, '2019-12-15 13:41:41', 0),
(208, 16, 9, 6, '2019-12-15 13:41:41', 0),
(209, 52, 24, 30, '2019-12-15 13:41:41', 0),
(210, 34, 4, 19, '2019-12-15 13:41:41', 0),
(211, 23, 6, 31, '2019-12-15 13:41:41', 0),
(212, 16, 17, 10, '2019-12-15 13:41:41', 0),
(213, 44, 13, 23, '2019-12-15 13:41:41', 0),
(214, 20, 4, 18, '2019-12-15 13:41:41', 0),
(215, 61, 10, 31, '2019-12-15 13:41:41', 0),
(216, 96, 16, 32, '2019-12-15 13:41:41', 0),
(217, 89, 22, 28, '2019-12-15 13:41:41', 0),
(218, 7, 16, 40, '2019-12-15 13:41:41', 0),
(219, 89, 5, 6, '2019-12-15 13:41:41', 0),
(220, 34, 23, 30, '2019-12-15 13:41:41', 0),
(221, 10, 19, 10, '2019-12-15 13:41:41', 0),
(222, 76, 14, 1, '2019-12-15 13:41:41', 0),
(223, 38, 14, 22, '2019-12-15 13:41:41', 0),
(224, 72, 19, 40, '2019-12-15 13:41:41', 0),
(225, 75, 2, 40, '2019-12-15 13:41:41', 0),
(226, 12, 14, 40, '2019-12-15 13:41:41', 0),
(227, 93, 14, 6, '2019-12-15 13:41:41', 0),
(228, 99, 13, 2, '2019-12-15 13:41:41', 0),
(229, 17, 11, 29, '2019-12-15 13:41:41', 0),
(230, 18, 3, 11, '2019-12-15 13:41:41', 0),
(231, 34, 8, 7, '2019-12-15 13:41:41', 0),
(232, 28, 1, 9, '2019-12-15 13:41:41', 0),
(233, 84, 1, 14, '2019-12-15 13:41:41', 0),
(234, 55, 14, 36, '2019-12-15 13:41:41', 0),
(235, 95, 27, 21, '2019-12-15 13:41:41', 0),
(236, 95, 13, 13, '2019-12-15 13:41:41', 0),
(237, 82, 1, 1, '2019-12-15 13:41:41', 0),
(238, 10, 3, 10, '2019-12-15 13:41:41', 0),
(239, 85, 28, 21, '2019-12-15 13:41:41', 0),
(240, 42, 9, 20, '2019-12-15 13:41:41', 0),
(241, 18, 13, 39, '2019-12-15 13:41:41', 0),
(242, 18, 15, 10, '2019-12-15 13:41:41', 0),
(243, 11, 17, 16, '2019-12-15 13:41:41', 0),
(244, 58, 2, 11, '2019-12-15 13:41:41', 0),
(245, 53, 14, 29, '2019-12-15 13:41:41', 0),
(246, 33, 24, 28, '2019-12-15 13:41:41', 0),
(247, 68, 21, 23, '2019-12-15 13:41:41', 0),
(248, 93, 11, 38, '2019-12-15 13:41:41', 0),
(249, 31, 7, 7, '2019-12-15 13:41:41', 0),
(250, 92, 19, 10, '2019-12-15 13:41:41', 0),
(251, 93, 14, 18, '2019-12-15 13:41:41', 0),
(252, 30, 23, 31, '2019-12-15 13:41:41', 0),
(253, 42, 13, 4, '2019-12-15 13:41:41', 0),
(254, 68, 2, 26, '2019-12-15 13:41:41', 0),
(255, 95, 14, 24, '2019-12-15 13:41:41', 0),
(256, 74, 11, 27, '2019-12-15 13:41:41', 0),
(257, 61, 27, 19, '2019-12-15 13:41:41', 0),
(258, 65, 18, 8, '2019-12-15 13:41:41', 0),
(259, 92, 2, 34, '2019-12-15 13:41:41', 0),
(260, 90, 8, 6, '2019-12-15 13:41:41', 0),
(261, 84, 26, 23, '2019-12-15 13:41:41', 0),
(262, 79, 8, 4, '2019-12-15 13:41:41', 0),
(263, 86, 4, 26, '2019-12-15 13:41:41', 0),
(264, 64, 30, 33, '2019-12-15 13:41:41', 0),
(265, 84, 8, 18, '2019-12-15 13:41:41', 0),
(266, 77, 19, 15, '2019-12-15 13:41:41', 0),
(267, 49, 26, 24, '2019-12-15 13:41:41', 0),
(268, 26, 25, 27, '2019-12-15 13:41:41', 0),
(269, 47, 19, 32, '2019-12-15 13:41:41', 0),
(270, 54, 28, 16, '2019-12-15 13:41:41', 0),
(271, 25, 22, 17, '2019-12-15 13:41:41', 0),
(272, 8, 15, 24, '2019-12-15 13:41:41', 0),
(273, 42, 27, 30, '2019-12-15 13:41:41', 0),
(274, 31, 1, 27, '2019-12-15 13:41:41', 0),
(275, 31, 13, 20, '2019-12-15 13:41:41', 0),
(276, 89, 18, 19, '2019-12-15 13:41:41', 0),
(277, 43, 11, 30, '2019-12-15 13:41:41', 0),
(278, 59, 1, 18, '2019-12-15 13:41:41', 0),
(279, 13, 10, 18, '2019-12-15 13:41:41', 0),
(280, 18, 18, 23, '2019-12-15 13:41:41', 0),
(281, 90, 26, 27, '2019-12-15 13:41:41', 0),
(282, 46, 15, 11, '2019-12-15 13:41:41', 0),
(283, 53, 11, 5, '2019-12-15 13:41:41', 0),
(284, 39, 12, 26, '2019-12-15 13:41:41', 0),
(285, 47, 23, 14, '2019-12-15 13:41:41', 0),
(286, 28, 24, 26, '2019-12-15 13:41:41', 0),
(287, 97, 21, 11, '2019-12-15 13:41:41', 0),
(288, 52, 8, 19, '2019-12-15 13:41:41', 0),
(289, 80, 23, 6, '2019-12-15 13:41:41', 0),
(290, 46, 18, 29, '2019-12-15 13:41:41', 0),
(291, 40, 7, 36, '2019-12-15 13:41:41', 0),
(292, 19, 9, 30, '2019-12-15 13:41:41', 0),
(293, 17, 23, 13, '2019-12-15 13:41:41', 0),
(294, 95, 3, 27, '2019-12-15 13:41:41', 0),
(295, 41, 15, 9, '2019-12-15 13:41:41', 0),
(296, 58, 13, 31, '2019-12-15 13:41:41', 0),
(297, 30, 26, 5, '2019-12-15 13:41:41', 0),
(298, 19, 9, 6, '2019-12-15 13:41:41', 0),
(299, 38, 8, 26, '2019-12-15 13:41:41', 0),
(300, 18, 1, 28, '2019-12-15 13:41:41', 0),
(301, 18, 15, 10, '2019-12-15 13:41:41', 0);

-- --------------------------------------------------------

--
-- Table structure for table `raw_materials`
--

CREATE TABLE `raw_materials` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `buy_qty` float NOT NULL,
  `buy_price` int(11) NOT NULL,
  `uom` varchar(20) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `raw_materials`
--

INSERT INTO `raw_materials` (`id`, `code`, `name`, `buy_qty`, `buy_price`, `uom`, `unit_price`, `created_at`, `is_deleted`) VALUES
(1, '51655-956', 'Juice Peach Nectar', 322, 12696, 'pcs', 39, '2019-12-15 12:21:55', 0),
(2, '25021-201', 'Langers - Cranberry Cocktail', 985, 55768, 'pcs', 56, '2019-12-15 12:21:55', 0),
(3, '50436-7000', 'Wine - Placido Pinot Grigo', 181, 21530, 'm', 118, '2019-12-15 12:21:55', 0),
(6, '36987-2906', 'Pears - Bosc', 659, 52024, 'pcs', 78, '2019-12-15 12:22:20', 0),
(7, '64980-151', 'Tea - Darjeeling, Azzura', 109, 81868, 'cm', 751, '2019-12-15 12:22:20', 0),
(8, '36987-1741', 'Lamb - Racks, Frenched', 526, 40527, 'cm', 77, '2019-12-15 12:22:20', 0),
(9, '65862-164', 'Shrimp - 16/20, Peeled Deviened', 588, 44869, 'm', 76, '2019-12-15 12:22:20', 0),
(10, '63629-3342', 'Muffin Mix - Raisin Bran', 576, 78727, 'cm', 136, '2019-12-15 12:22:20', 0),
(11, '57955-5984', 'Beans - Turtle, Black, Dry', 433, 94097, 'm', 217, '2019-12-15 12:22:20', 0),
(12, '0093-2056', 'Cheese - Pont Couvert', 819, 40383, 'm', 49, '2019-12-15 12:22:20', 0),
(13, '52584-977', 'Gatorade - Lemon Lime', 606, 68064, 'cm', 112, '2019-12-15 12:22:20', 0),
(14, '0135-0242', 'Bread - White Epi Baguette', 701, 51393, 'cm', 73, '2019-12-15 12:22:20', 0),
(15, '68745-1133', 'Wine - Maipo Valle Cabernet', 216, 76486, 'roll', 354, '2019-12-15 12:22:20', 0),
(16, '40046-0064', 'Cheese - Perron Cheddar', 262, 36861, 'cm', 140, '2019-12-15 12:22:20', 0),
(17, '49999-038', 'Chick Peas - Dried', 225, 52397, 'cm', 232, '2019-12-15 12:22:20', 0),
(18, '65862-011', 'Pineapple - Regular', 908, 14591, 'cm', 16, '2019-12-15 12:22:20', 0),
(19, '68788-6762', 'Cherries - Bing, Canned', 611, 64146, 'cm', 104, '2019-12-15 12:22:20', 0),
(20, '36000-002', 'Curry Paste - Green Masala', 100, 25234, 'm', 252, '2019-12-15 12:22:20', 0),
(21, '68428-018', 'Dehydrated Kelp Kombo', 103, 57951, 'pcs', 562, '2019-12-15 12:22:20', 0),
(22, '51444-002', 'Beef - Short Loin', 506, 83050, 'm', 164, '2019-12-15 12:22:20', 0),
(23, '54868-3742', 'Swiss Chard - Red', 297, 73467, 'pcs', 247, '2019-12-15 12:22:20', 0),
(24, '63739-457', 'Filter - Coffee', 903, 65460, 'cm', 72, '2019-12-15 12:22:20', 0),
(25, '0113-0763', 'Bols Melon Liqueur', 988, 52418, 'cm', 53, '2019-12-15 12:22:20', 0),
(26, '0378-9122', 'Wine - Red, Cabernet Sauvignon', 471, 60012, 'cm', 127, '2019-12-15 12:22:20', 0),
(27, '0536-3755', 'Pepper - Pablano', 785, 70179, 'cm', 89, '2019-12-15 12:22:20', 0),
(28, '63776-415', 'Pepper - Cayenne', 483, 98556, 'm', 204, '2019-12-15 12:22:20', 0),
(29, '57520-0209', 'Bread - Triangle White', 290, 24682, 'roll', 85, '2019-12-15 12:22:20', 0),
(30, '51862-216', 'Everfresh Products', 561, 12268, 'pcs', 21, '2019-12-15 12:22:20', 0),
(31, '33992-4951', 'Tart - Raisin And Pecan', 419, 72394, 'cm', 172, '2019-12-15 12:22:20', 0),
(32, '52584-652', 'Chinese Foods - Cantonese', 822, 46822, 'roll', 56, '2019-12-15 12:22:20', 0),
(33, '0093-7437', 'Towel Multifold', 321, 12516, 'm', 38, '2019-12-15 12:22:20', 0),
(34, '55910-958', 'Cake - Box Window 10x10x2.5', 270, 34984, 'pcs', 129, '2019-12-15 12:22:20', 0),
(35, '68400-121', 'Flour - Strong Pizza', 219, 71049, 'cm', 324, '2019-12-15 12:22:20', 0),
(36, '68016-532', 'Bread - Triangle White', 384, 19248, 'pcs', 50, '2019-12-15 12:22:20', 0),
(37, '52343-044', 'Coke - Diet, 355 Ml', 305, 94346, 'roll', 309, '2019-12-15 12:22:20', 0),
(38, '0713-0635', 'Juice - V8 Splash', 900, 64238, 'roll', 71, '2019-12-15 12:22:20', 0),
(39, '67938-0858', 'Coconut Milk - Unsweetened', 508, 25419, 'pcs', 50, '2019-12-15 12:22:20', 0),
(40, '51386-722', 'Sprouts - Baby Pea Tendrils', 988, 15191, 'pcs', 15, '2019-12-15 12:22:20', 0),
(41, '68645-282', 'Cookies - Fortune', 158, 61506, 'pcs', 389, '2019-12-15 12:22:20', 0),
(42, '0781-2148', 'Mortadella', 863, 66085, 'roll', 76, '2019-12-15 12:22:20', 0),
(43, '54569-5750', 'Venison - Ground', 214, 81777, 'cm', 382, '2019-12-15 12:22:20', 0),
(44, '53145-052', 'Cleaner - Comet', 841, 36096, 'pcs', 42, '2019-12-15 12:22:20', 0),
(45, '68001-230', 'Vodka - Hot, Lnferno', 487, 94444, 'pcs', 193, '2019-12-15 12:22:20', 0),
(46, '0228-2779', 'Wine - Shiraz Wolf Blass Premium', 370, 77742, 'm', 210, '2019-12-15 12:22:20', 0),
(47, '58118-8344', 'Wine - Rioja Campo Viejo', 233, 29287, 'cm', 125, '2019-12-15 12:22:20', 0),
(48, '53499-5146', 'Lettuce - Curly Endive', 277, 80043, 'm', 288, '2019-12-15 12:22:20', 0),
(49, '0268-6180', 'Chips - Potato Jalapeno', 191, 50090, 'm', 262, '2019-12-15 12:22:20', 0),
(50, '67046-428', 'Fond - Chocolate', 604, 67066, 'cm', 111, '2019-12-15 12:22:20', 0),
(51, '10096-0269', 'Apple - Macintosh', 237, 41347, 'roll', 174, '2019-12-15 12:22:20', 0),
(52, '50268-264', 'Wine - Black Tower Qr', 909, 95292, 'pcs', 104, '2019-12-15 12:22:20', 0),
(53, '52125-095', 'Veal - Insides, Grains', 766, 16584, 'm', 21, '2019-12-15 12:22:20', 0),
(54, '54868-5103', 'Salmon - Fillets', 954, 51674, 'pcs', 54, '2019-12-15 12:22:20', 0),
(55, '36800-807', 'Tea - Honey Green Tea', 671, 35092, 'm', 52, '2019-12-15 12:22:20', 0),
(56, '46122-141', 'Beef - Striploin Aa', 273, 17419, 'cm', 63, '2019-12-15 12:22:20', 0),
(57, '0093-4155', 'Anisette - Mcguiness', 339, 35447, 'cm', 104, '2019-12-15 12:22:20', 0),
(58, '62207-750', 'Sugar - Cubes', 928, 87842, 'cm', 94, '2019-12-15 12:22:20', 0),
(59, '68382-098', 'Filo Dough', 256, 29685, 'pcs', 115, '2019-12-15 12:22:20', 0),
(60, '41163-689', 'Wine - Ruffino Chianti Classico', 648, 28375, 'roll', 43, '2019-12-15 12:22:20', 0),
(61, '43063-271', 'Parsnip', 609, 36484, 'm', 59, '2019-12-15 12:22:20', 0),
(62, '0219-3103', 'Pastry - Lemon Danish - Mini', 702, 25492, 'pcs', 36, '2019-12-15 12:22:20', 0),
(63, '0093-7147', 'Wine - Acient Coast Caberne', 203, 52901, 'cm', 260, '2019-12-15 12:22:20', 0),
(64, '49348-983', 'Crackers - Soda / Saltins', 824, 71526, 'm', 86, '2019-12-15 12:22:20', 0),
(65, '49817-1988', 'Pastry - Cherry Danish - Mini', 727, 72085, 'cm', 99, '2019-12-15 12:22:20', 0),
(66, '12634-166', 'Fennel', 317, 75680, 'pcs', 238, '2019-12-15 12:22:20', 0),
(67, '55154-6964', 'Sponge Cake Mix - Vanilla', 846, 22254, 'cm', 26, '2019-12-15 12:22:20', 0),
(68, '60505-2542', 'Green Tea Refresher', 988, 56419, 'roll', 57, '2019-12-15 12:22:20', 0),
(69, '43353-815', 'Compound - Pear', 799, 56304, 'roll', 70, '2019-12-15 12:22:20', 0),
(70, '48433-311', 'Lemonade - Island Tea, 591 Ml', 562, 85453, 'cm', 152, '2019-12-15 12:22:20', 0),
(71, '43251-3345', 'Bar Bran Honey Nut', 224, 80495, 'roll', 359, '2019-12-15 12:22:20', 0),
(72, '42953-000', 'Paper Towel Touchless', 765, 73534, 'm', 96, '2019-12-15 12:22:20', 0),
(73, '52959-629', 'Horseradish - Prepared', 677, 92162, 'cm', 136, '2019-12-15 12:22:20', 0),
(74, '58194-014', 'Carbonated Water - Lemon Lime', 664, 41981, 'pcs', 63, '2019-12-15 12:22:20', 0),
(75, '30142-952', 'Cheese - Brie, Cups 125g', 721, 54731, 'pcs', 75, '2019-12-15 12:22:20', 0),
(76, '63287-425', 'Sauce Bbq Smokey', 283, 14180, 'pcs', 50, '2019-12-15 12:22:20', 0),
(77, '0781-9125', 'Appetizer - Shrimp Puff', 827, 56245, 'm', 68, '2019-12-15 12:22:20', 0),
(78, '49884-678', 'Shallots', 677, 92017, 'm', 135, '2019-12-15 12:22:20', 0),
(79, '60512-6037', 'Yams', 883, 74273, 'm', 84, '2019-12-15 12:22:20', 0),
(80, '41163-264', 'Shrimp - Baby, Warm Water', 236, 28726, 'm', 121, '2019-12-15 12:22:20', 0),
(81, '0904-6187', 'Calvados - Boulard', 632, 62314, 'pcs', 98, '2019-12-15 12:22:20', 0),
(82, '55289-310', 'Cheese - Augre Des Champs', 687, 91097, 'm', 132, '2019-12-15 12:22:20', 0),
(83, '0781-9408', 'Chocolate - Chips Compound', 652, 46707, 'roll', 71, '2019-12-15 12:22:20', 0),
(84, '30142-405', 'Juice - Apple, 341 Ml', 187, 52405, 'roll', 280, '2019-12-15 12:22:20', 0),
(85, '0555-9034', 'Pasta - Penne, Lisce, Dry', 837, 57579, 'roll', 68, '2019-12-15 12:22:20', 0),
(86, '58118-0169', 'Chinese Lemon Pork', 840, 64668, 'pcs', 76, '2019-12-15 12:22:20', 0),
(87, '50790-100', 'Bread - Malt', 882, 72895, 'm', 82, '2019-12-15 12:22:20', 0),
(88, '60681-2501', 'Pepper - Black, Whole', 429, 92876, 'm', 216, '2019-12-15 12:22:20', 0),
(89, '48951-8229', 'Nut - Cashews, Whole, Raw', 548, 22216, 'cm', 40, '2019-12-15 12:22:20', 0),
(90, '24236-594', 'Squash - Butternut', 818, 86350, 'pcs', 105, '2019-12-15 12:22:20', 0),
(91, '49999-083', 'Turkey Leg With Drum And Thigh', 591, 81978, 'roll', 138, '2019-12-15 12:22:20', 0),
(92, '58118-2272', 'Glass - Juice Clear 5oz 55005', 647, 37222, 'cm', 57, '2019-12-15 12:22:20', 0),
(93, '0904-6269', 'Ham - Cooked', 830, 18849, 'm', 22, '2019-12-15 12:22:20', 0),
(94, '65862-030', 'Apple - Macintosh', 306, 99816, 'm', 326, '2019-12-15 12:22:20', 0),
(95, '66689-711', 'Dill - Primerba, Paste', 965, 16518, 'cm', 17, '2019-12-15 12:22:20', 0),
(96, '55651-264', 'Syrup - Kahlua Chocolate', 723, 69331, 'cm', 95, '2019-12-15 12:22:20', 0),
(97, '41167-0936', 'Cheese - Mix', 709, 64230, 'cm', 90, '2019-12-15 12:22:20', 0),
(98, '64942-1288', 'Nantucket Pine Orangebanana', 617, 86759, 'roll', 140, '2019-12-15 12:22:20', 0),
(99, '43742-0217', 'Coke - Diet, 355 Ml', 813, 68691, 'pcs', 84, '2019-12-15 12:22:20', 0),
(100, '65954-002', 'Wine - Carmenere Casillero Del', 426, 79498, 'pcs', 186, '2019-12-15 12:22:20', 0),
(101, 'R545', 'Benang Merah', 1000, 10000, 'cm', 10, '2019-12-19 15:18:18', 0),
(102, 'R545', 'Benang Merah', 0, 0, 'cm', 10, '2019-12-19 15:18:48', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL DEFAULT '0',
  `last_name` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(222) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `password`) VALUES
(3, 'Lionel', 'Messi', 'haha'),
(4, 'Febri', 'Abidin', 'asd');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_hpp_details`
--
CREATE TABLE `v_hpp_details` (
`id` int(11)
,`hpp_id` int(11)
,`name` varchar(255)
,`code` varchar(255)
,`qty` int(11)
,`unit_price` int(11)
,`default_price` bigint(21)
,`uom` varchar(20)
,`created_at` timestamp
,`is_deleted` int(11)
);

-- --------------------------------------------------------

--
-- Structure for view `v_hpp_details`
--
DROP TABLE IF EXISTS `v_hpp_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_hpp_details`  AS  select `h`.`id` AS `id`,`h`.`hpp_id` AS `hpp_id`,`r`.`name` AS `name`,`r`.`code` AS `code`,`h`.`qty` AS `qty`,`r`.`unit_price` AS `unit_price`,(`h`.`qty` * `r`.`unit_price`) AS `default_price`,`r`.`uom` AS `uom`,`h`.`created_at` AS `created_at`,`h`.`is_deleted` AS `is_deleted` from (`hpp_details` `h` join `raw_materials` `r` on((`h`.`raw_id` = `r`.`id`))) order by `h`.`hpp_id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hpps`
--
ALTER TABLE `hpps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hpp_details`
--
ALTER TABLE `hpp_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raw_materials`
--
ALTER TABLE `raw_materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `hpps`
--
ALTER TABLE `hpps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `hpp_details`
--
ALTER TABLE `hpp_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=302;
--
-- AUTO_INCREMENT for table `raw_materials`
--
ALTER TABLE `raw_materials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
