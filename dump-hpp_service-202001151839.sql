-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: hpp_service
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(222) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Kemeja Putih','2001','2019-12-23 08:56:23',0),(2,'Outer Merah','2002','2019-12-23 08:56:23',0),(3,'Blazzer Navy','2003','2019-12-23 08:56:23',0),(4,'Kerudung Segitia','2004','2019-12-23 08:56:23',0),(5,'Kaos Hijau','2005','2019-12-23 08:56:23',0),(6,'Hijab Bulat','2006','2019-12-31 11:14:05',0);
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hpp_details`
--

DROP TABLE IF EXISTS `hpp_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hpp_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `raw_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `hpp_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hpp_details`
--

LOCK TABLES `hpp_details` WRITE;
/*!40000 ALTER TABLE `hpp_details` DISABLE KEYS */;
INSERT INTO `hpp_details` VALUES (1,19,22,1,'2019-12-23 12:08:15',0),(2,18,13,2,'2019-12-23 12:08:15',0),(3,19,27,3,'2019-12-23 12:08:15',0),(4,20,92,4,'2019-12-23 12:08:15',0),(5,20,27,5,'2019-12-23 12:08:15',0),(6,20,30,6,'2019-12-23 12:08:15',0),(7,18,81,7,'2019-12-23 12:08:15',0),(8,20,52,8,'2019-12-23 12:08:15',0),(9,15,12,9,'2019-12-23 12:08:15',0),(10,17,13,10,'2019-12-23 12:08:15',0),(11,19,26,11,'2019-12-23 12:08:15',0),(12,16,42,12,'2019-12-23 12:08:15',0),(13,18,38,13,'2019-12-23 12:08:15',0),(14,16,32,14,'2019-12-23 12:08:15',0),(15,14,42,15,'2019-12-23 12:08:15',0),(16,23,64,1,'2019-12-23 12:08:15',0),(17,25,65,2,'2019-12-23 12:08:15',0),(18,22,88,3,'2019-12-23 12:08:15',0),(19,25,19,4,'2019-12-23 12:08:15',0),(20,22,32,5,'2019-12-23 12:08:15',0),(21,21,70,6,'2019-12-23 12:08:15',0),(22,21,47,7,'2019-12-23 12:08:15',0),(23,21,44,8,'2019-12-23 12:08:15',0),(24,21,12,9,'2019-12-23 12:08:15',0),(25,25,95,10,'2019-12-23 12:08:15',0),(26,22,69,11,'2019-12-23 12:08:15',0),(27,22,79,12,'2019-12-23 12:08:15',0),(28,22,99,13,'2019-12-23 12:08:15',0),(29,24,42,14,'2019-12-23 12:08:15',0),(30,24,50,15,'2019-12-23 12:08:15',0),(31,29,45,1,'2019-12-23 12:08:15',0),(32,26,23,2,'2019-12-23 12:08:15',0),(33,26,39,3,'2019-12-23 12:08:15',0),(34,28,21,4,'2019-12-23 12:08:15',0),(35,28,90,5,'2019-12-23 12:08:15',0),(36,28,17,6,'2019-12-23 12:08:15',0),(37,30,100,7,'2019-12-23 12:08:15',0),(38,28,91,8,'2019-12-23 12:08:15',0),(39,30,22,9,'2019-12-23 12:08:15',0),(40,27,100,10,'2019-12-23 12:08:15',0),(41,27,86,11,'2019-12-23 12:08:15',0),(42,26,28,12,'2019-12-23 12:08:15',0),(43,29,42,13,'2019-12-23 12:08:15',0),(44,26,14,14,'2019-12-23 12:08:15',0),(45,29,76,15,'2019-12-23 12:08:15',0),(46,33,58,1,'2019-12-23 12:08:15',0),(47,33,74,2,'2019-12-23 12:08:15',0),(48,33,17,3,'2019-12-23 12:08:15',0),(49,35,73,4,'2019-12-23 12:08:15',0),(50,33,16,5,'2019-12-23 12:08:15',0),(51,34,44,6,'2019-12-23 12:08:15',0),(52,32,40,7,'2019-12-23 12:08:15',0),(53,31,97,8,'2019-12-23 12:08:15',0),(54,34,95,9,'2019-12-23 12:08:15',0),(55,33,53,10,'2019-12-23 12:08:15',0),(56,32,68,11,'2019-12-23 12:08:15',0),(57,31,99,12,'2019-12-23 12:08:15',0),(58,33,81,13,'2019-12-23 12:08:15',0),(59,35,88,14,'2019-12-23 12:08:15',0),(60,32,97,15,'2019-12-23 12:08:15',0),(61,34,0,19,'2019-12-31 10:51:19',1),(62,14,0,19,'2019-12-31 10:51:16',1),(63,35,0,20,'2019-12-31 10:54:30',1),(64,35,0,18,'2019-12-31 10:59:40',1),(65,35,0,20,'2019-12-31 11:13:29',1),(66,35,0,19,'2019-12-31 11:13:21',1),(67,17,0,19,'2019-12-31 11:13:17',1),(68,35,21,20,'2019-12-31 11:15:09',0);
/*!40000 ALTER TABLE `hpp_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hpps`
--

DROP TABLE IF EXISTS `hpps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hpps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `name` varchar(123) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hpps`
--

LOCK TABLES `hpps` WRITE;
/*!40000 ALTER TABLE `hpps` DISABLE KEYS */;
INSERT INTO `hpps` VALUES (1,1,'Kemeja Putih American Style',0,'2019-12-23 11:40:01'),(2,2,'Outer Merah American Style',0,'2019-12-23 11:40:01'),(3,3,'Blazzer Navy American Style',0,'2019-12-23 11:40:01'),(4,4,'Kerudung Segitia American Style',0,'2019-12-23 11:40:01'),(5,5,'Kaos Hijau American Style',0,'2019-12-23 11:40:01'),(6,1,'Kemeja Putih British Style',0,'2019-12-23 11:40:01'),(7,2,'Outer Merah British Style',0,'2019-12-23 11:40:01'),(8,3,'Blazzer Navy British Style',0,'2019-12-23 11:40:01'),(9,4,'Kerudung Segitia British Style',0,'2019-12-23 11:40:01'),(10,5,'Kaos Hijau British Style',0,'2019-12-23 11:40:01'),(11,1,'Kemeja Putih Mexican Style',0,'2019-12-23 11:40:01'),(12,2,'Outer Merah Mexican Style',0,'2019-12-23 11:40:01'),(13,3,'Blazzer Navy Mexican Style',0,'2019-12-23 11:40:01'),(14,4,'Kerudung Segitia Mexican Style',0,'2019-12-23 11:40:01'),(15,5,'Kaos Hijau Mexican Style',0,'2019-12-23 11:40:01'),(16,6,'ads',0,'2019-12-25 09:05:16'),(17,6,'asd',0,'2019-12-25 09:10:42'),(18,6,'jaja',0,'2019-12-30 03:09:07'),(19,6,'dd',0,'2019-12-30 05:52:44'),(20,6,'22',0,'2019-12-30 06:16:24');
/*!40000 ALTER TABLE `hpps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `raw_materials`
--

DROP TABLE IF EXISTS `raw_materials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raw_materials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `buy_qty` float NOT NULL,
  `buy_price` int(11) NOT NULL,
  `uom` varchar(20) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raw_materials`
--

LOCK TABLES `raw_materials` WRITE;
/*!40000 ALTER TABLE `raw_materials` DISABLE KEYS */;
INSERT INTO `raw_materials` VALUES (14,'1001','Kain Linen',20,30000,'M',1500,'2019-12-23 08:50:02',0),(15,'1002','Kain Wool',90,70000,'M',780,'2019-12-23 08:50:02',0),(16,'1003','Kain Sutra',60,90000,'M',1500,'2019-12-23 08:50:02',0),(17,'1004','Kain Katun',30,20000,'M',670,'2019-12-23 08:50:02',0),(18,'1005','Kain Denim',80,100000,'M',1250,'2019-12-23 08:50:02',0),(19,'1006','Kain Canvas',80,100000,'M',1250,'2019-12-23 08:50:02',0),(20,'1007','Kain Suede',70,50000,'M',710,'2019-12-23 08:50:02',0),(21,'1008','Kain Lycra',90,40000,'M',440,'2019-12-23 08:50:02',0),(22,'1009','Kain Polyester',40,10000,'M',250,'2019-12-23 08:50:02',0),(23,'1010','Kain Bludru',90,30000,'M',330,'2019-12-23 08:50:02',0),(24,'1011','Kain Jersey',90,20000,'M',220,'2019-12-23 08:50:02',0),(25,'1021','Kancing Lubang',80,70000,'Pcs',880,'2019-12-23 08:50:02',0),(26,'1022','Kancing Jepret',100,90000,'Pcs',900,'2019-12-23 08:50:02',0),(27,'1023','Kancing Sengkelit',50,70000,'Pcs',1400,'2019-12-23 08:50:02',0),(28,'1031','Benang Sulam',10,50000,'Cm',5000,'2019-12-23 08:50:02',0),(29,'1032','Benang Kasur',30,10000,'Cm',330,'2019-12-23 08:50:02',0),(30,'1033','Benang Nilon',10,40000,'Cm',4000,'2019-12-23 08:50:02',0),(31,'1034','Benang Bordir',100,10000,'Cm',100,'2019-12-23 08:50:02',0),(32,'1035','Benang Renda',30,10000,'Cm',330,'2019-12-23 08:50:02',0),(33,'1041','Resleting',100,70000,'Pcs',700,'2019-12-23 08:50:02',0),(34,'1042','Tali Kur',70,100000,'M',1430,'2019-12-23 08:50:02',0),(35,'1043','Tali Anyam',100,10000,'M',100,'2019-12-23 08:50:02',0);
/*!40000 ALTER TABLE `raw_materials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL DEFAULT '0',
  `last_name` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(222) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'Lionel','Messi','haha'),(4,'Febri','Abidin','asd');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_hpp_details`
--

DROP TABLE IF EXISTS `v_hpp_details`;
/*!50001 DROP VIEW IF EXISTS `v_hpp_details`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_hpp_details` AS SELECT 
 1 AS `id`,
 1 AS `hpp_id`,
 1 AS `raw_id`,
 1 AS `name`,
 1 AS `code`,
 1 AS `qty`,
 1 AS `unit_price`,
 1 AS `default_price`,
 1 AS `uom`,
 1 AS `created_at`,
 1 AS `is_deleted`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_hpps`
--

DROP TABLE IF EXISTS `v_hpps`;
/*!50001 DROP VIEW IF EXISTS `v_hpps`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_hpps` AS SELECT 
 1 AS `id`,
 1 AS `article_id`,
 1 AS `name`,
 1 AS `is_deleted`,
 1 AS `created_at`,
 1 AS `hpp_price`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'hpp_service'
--

--
-- Final view structure for view `v_hpp_details`
--

/*!50001 DROP VIEW IF EXISTS `v_hpp_details`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_hpp_details` AS select `h`.`id` AS `id`,`h`.`hpp_id` AS `hpp_id`,`r`.`id` AS `raw_id`,`r`.`name` AS `name`,`r`.`code` AS `code`,`h`.`qty` AS `qty`,`r`.`unit_price` AS `unit_price`,(`h`.`qty` * `r`.`unit_price`) AS `default_price`,`r`.`uom` AS `uom`,`h`.`created_at` AS `created_at`,`h`.`is_deleted` AS `is_deleted` from (`hpp_details` `h` join `raw_materials` `r` on((`h`.`raw_id` = `r`.`id`))) order by `h`.`hpp_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_hpps`
--

/*!50001 DROP VIEW IF EXISTS `v_hpps`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_hpps` AS select `a`.`id` AS `id`,`a`.`article_id` AS `article_id`,`a`.`name` AS `name`,`a`.`is_deleted` AS `is_deleted`,`a`.`created_at` AS `created_at`,sum(`b`.`default_price`) AS `hpp_price` from ((`hpps` `a` left join `v_hpp_details` `b` on((`a`.`id` = `b`.`hpp_id`))) join `articles` `c` on((`a`.`article_id` = `c`.`id`))) group by `a`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-15 18:39:41
