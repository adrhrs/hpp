'use strict'

module.exports = function(app) {
    var cont = require('./controller')

    app.route('/')
        .get(cont.index)


    //generic
    

    app.route('/list/:table_name/:page')
        .get(cont.listGeneric)

    app.route('/cond/:table_name/:page')
        .post(cont.conditionalGeneric)    

    app.route('/find/:table_name/:id')
        .get(cont.findGeneric)

    app.route('/create/:table_name')
        .post(cont.createGeneric)

    app.route('/update/:table_name/:id')
        .put(cont.updateGeneric)

}